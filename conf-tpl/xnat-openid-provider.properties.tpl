name=OpenId
auth.method=openid
provider.id=openid
visible=true
auto.enabled=true
auto.verified=true
disableUsernamePasswordLogin=false
siteUrl={{ .Env.XNAT_ADMIN_URL }}
preEstablishedRedirUri=/openid-login

enabled=keycloak
openid.keycloak.clientId={{ .Env.KEYCLOAK_PUBLIC_CLIENT }}
openid.keycloak.clientSecret=
openid.keycloak.accessTokenUri={{ .Env.XNAT_OPENID_ACCESS_TOKEN_BASE_URL }}/auth/realms/snet/protocol/openid-connect/token
openid.keycloak.userAuthUri={{ .Env.XNAT_OPENID_USER_AUTH_BASE_URL }}/auth/realms/snet/protocol/openid-connect/auth
openid.keycloak.scopes=openid,profile,email
openid.keycloak.link=<p>To sign-in using your Keycloak credentials, please click on the button below.</p></p><p><a href="/xnat/openid-login?providerId=keycloak">Sign-in with Keycloak</a></p>
openid.keycloak.shouldFilterEmailDomains=false
openid.keycloak.forceUserCreate=true
openid.keycloak.userAutoEnabled=true
openid.keycloak.userAutoVerified=true
openid.keycloak.emailProperty=email
openid.keycloak.givenNameProperty=given_name
openid.keycloak.familyNameProperty=family_name
