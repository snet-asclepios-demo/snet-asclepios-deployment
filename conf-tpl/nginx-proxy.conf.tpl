proxy_http_version 1.1;

# Increase proxy buffer size to resolve the following error:
# [error] 115#115: *1008 upstream sent too big header while reading response
# header from upstream, client: 172.19.0.1, server: keycloak.snet-apps.local,
# request: "GET
# /auth/realms/snet/protocol/openid-connect/auth?client_id=calls-gateway&redirect_uri=http%3A%2F%2Fxnat.snet-apps.local%2Fasclepios-search%2F%3Ffilename%3Dasclepiostestproject%2FXNAT5_S00001%2FXNAT7_E00005%2Fbd394520-aeb6-4ace-b6f4-95845f726aab.edf%26keyid%3D2b123924-50d7-4e98-a68e-f1970eb7fa49%23get&state=e2ffc884-d2d0-4b0e-978e-93892fc37107&response_mode=fragment&response_type=code&scope=openid&nonce=a48f6a53-d5f4-4498-8680-3f257edf66a6
# HTTP/1.1", upstream:
# "http://172.19.0.13:8181/auth/realms/snet/protocol/openid-connect/auth?client_id=calls-gateway&redirect_uri=http%3A%2F%2Fxnat.snet-apps.local%2Fasclepios-search%2F%3Ffilename%3Dasclepiostestproject%2FXNAT5_S00001%2FXNAT7_E00005%2Fbd394520-aeb6-4ace-b6f4-95845f726aab.edf%26keyid%3D2b123924-50d7-4e98-a68e-f1970eb7fa49%23get&state=e2ffc884-d2d0-4b0e-978e-93892fc37107&response_mode=fragment&response_type=code&scope=openid&nonce=a48f6a53-d5f4-4498-8680-3f257edf66a6",
# host: "keycloak.snet-apps.local", referrer: "http://xnat.snet-apps.local/"

proxy_buffering on;
proxy_buffers 4 256k;
proxy_buffer_size 128k;
proxy_busy_buffers_size 256k;

proxy_set_header Host $http_host;
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection $proxy_connection;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto $proxy_x_forwarded_proto;
proxy_set_header X-Forwarded-Ssl $proxy_x_forwarded_ssl;
proxy_set_header X-Forwarded-Port $proxy_x_forwarded_port;

# Mitigate httpoxy attack (see README for details)
# NOTE: The following configuration blocks the Proxy HTTP request header from
# being sent to downstream servers. This prevents attackers from using the
# so-called httpoxy attack. There is no legitimate reason for a client to send
# this header, and there are many vulnerable languages / platforms
# (CVE-2016-5385, CVE-2016-5386, CVE-2016-5387, CVE-2016-5388, CVE-2016-1000109,
# CVE-2016-1000110, CERT-VU#797896).

proxy_set_header Proxy "";
