[siteConfig]
siteId=snet-asclepios-demo
siteUrl={{ .Env.XNAT_ADMIN_URL }}
processingUrl=http:xnat:8080/xnat
adminEmail=bowden@htw-berlin.de
emailVerification=false
userRegistration=false
par=false
enableCsrfToken=false
uiAllowNonAdminProjectCreation=false
enabledProviders=["localdb", "openid"]

[notifications]
smtpEnabled=false
copyAdminOnPageEmails=false
copyAdminOnNotifications=false
