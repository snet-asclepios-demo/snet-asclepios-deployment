# Inputs removed

tosca_definitions_version: tosca_simple_yaml_1_2

imports:
  - https://raw.githubusercontent.com/micado-scale/tosca/develop/micado_types.yaml

repositories:
  docker_hub: https://hub.docker.com/

description: ADT for Somnonetz ASCLEPIOS storage platform on OpenStack

dsl_definitions:

  compute_cloud_with_nfs: &compute_cloud_with_nfs
    type: tosca.nodes.MiCADO.Nova.Compute
    properties:
      image_id: 9ec073d4-743d-4c7a-98eb-5be18d937d52
      flavor_name: m1.medium
      project_id: bedf16b8b98e41ea80ba3d1680b9d317
      network_id: c4a7ce20-5f68-4f48-9deb-3c2c6b52f397
      config_drive: true
      key_name: james-htw-key
      floating_ip_pool: public_net
      floating_ip: 161.74.31.91
      security_groups:
        - 94fa5200-7e4f-4e18-9df7-efb75e2c1d5e #MiCADO-worker UOW ONLY
        - d2ad0c7c-5f3a-4732-a04a-3885b5e5866a #default
        - 34cacbf2-d367-42e8-8edd-c567906fc231 #ports
        - fc6dabe0-a886-4fd0-a936-4bb3beb93b87 #public ports
      context:
        insert: true
        cloud_config: |
          runcmd:
          - apt-get install -y nfs-common
    interfaces:
      Terraform:
        create:
          inputs:
            network_name: cpc-dev_net
            endpoint: https://api1.cse.westminster.ac.uk:5000/v3

topology_template:

  policies:
  - monitoring:
      type: tosca.policies.Monitoring.MiCADO
      properties:
        enable_container_metrics: true
        enable_node_metrics: true

  node_templates:

    # Application Server
    application-server: *compute_cloud_with_nfs

    traefik:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: traefik:1.7
        ports:
        - containerPort: 80
          hostPort: 80
        - containerPort: 443
          hostPort: 443
        args:
        - --api
        - --kubernetes
        - --logLevel=INFO
        - --defaultentrypoints=http,https
        - --entrypoints=Name:http Address::80 Redirect.EntryPoint:https
        - --entrypoints=Name:https Address::443 TLS
      requirements:
      - host: application-server
      interfaces:
        Kubernetes:
          create:
            inputs:
              metadata:
                namespace: kube-system
              spec:
                template:
                  spec:
                    serviceAccountName: traefik-ingress
                    terminationGracePeriodSeconds: 60

    issuer:
      type: tosca.nodes.MiCADO.Kubernetes
      interfaces:
        Kubernetes:
          create:
            inputs:
              apiVersion: cert-manager.io/v1
              kind: ClusterIssuer
              metadata:
                name: letsencrypt
              spec:
                acme:
                  server: https://acme-v02.api.letsencrypt.org/directory
                  email: bowden@htw-berlin.de
                  privateKeySecretRef:
                    name: letsencrypt
                  solvers:
                  - http01:
                      ingress:
                        class: traefik

    certificate:
      type: tosca.nodes.MiCADO.Kubernetes
      interfaces:
        Kubernetes:
          create:
            inputs:
              apiVersion: cert-manager.io/v1
              kind: Certificate
              metadata:
                name: snet-apps.com
              spec:
                secretName: snet-apps.com-tls
                issuerRef:
                  name: letsencrypt
                  kind: ClusterIssuer
                commonName: snet-apps.com
                dnsNames:
                - snet-apps.com
                - zuul.snet-apps.com
                - xnat.snet-apps.com
                - minio.snet-apps.com

    ingress:
      type: tosca.nodes.MiCADO.Kubernetes
      interfaces:
        Kubernetes:
          create:
            inputs:
              apiVersion: extensions/v1beta1
              kind: Ingress
              metadata:
                name: traefik-ingress
                annotations:
                  kubernetes.io/ingress.class: traefik
                  cert-manager.io/cluster-issuer: letsencrypt
              spec:
                rules:
                - host: snet-apps.com
                  http:
                    paths:
                    - backend:
                        serviceName: landing
                        servicePort: 80
                - host: xnat.snet-apps.com
                  http:
                    paths:
                    - backend:
                        serviceName: xnat
                        servicePort: 8080
                - host: zuul.snet-apps.com
                  http:
                    paths:
                    - backend:
                        serviceName: abac-zuul-proxy
                        servicePort: 80
                - host: minio.snet-apps.com
                  http:
                    paths:
                    - backend:
                        serviceName: minio-gateway
                        servicePort: 9000
                tls:
                - hosts:
                  - snet-apps.com
                  - zuul.snet-apps.com
                  - xnat.snet-apps.com
                  - minio.snet-apps.com
                  secretName: snet-apps.com-tls

    landing:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: nginx:latest
        ports:
        - port: 80
        - port: 443
      requirements:
      - host: application-server

    minio-gateway:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: minio/minio
        command:
           - /bin/bash
           - -c
           - minio gateway s3 http://minio-s3-server:9000
        env:
         - name: MINIO_ROOT_USER
           value: "{{ .Env.MINIO_ROOT_USER }}"
         - name: MINIO_ROOT_PASSWORD
           value: "{{ .Env.MINIO_ROOT_PASSWORD }}"
        ports:
        - port: {{ .Env.INTERNAL_MINIO_PORT }}
        - containerPort: {{ .Env.INTERNAL_MINIO_PORT }}
      requirements:
      - host: application-server

    xnat:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: docker.gitlab.gwdg.de/snet-asclepios-demo/dockerfiles/somnonetz/snet-xnat-asclepios:50f7fffad0a4776ccb8d021dd8c8d4e4fcb147ce
        env:
          - name: XNAT_ROOT
            value: "{{ .Env.XNAT_ROOT }}"
          - name: XNAT_HOME
            value: "{{ .Env.XNAT_HOME }}"
          - name: XNAT_DATASOURCE_DRIVER
            value: "{{ .Env.XNAT_DATASOURCE_DRIVER }}"
          - name: XNAT_DATASOURCE_URL
            value: "{{ .Env.XNAT_DATASOURCE_URL }}"
          - name: XNAT_DATASOURCE_USERNAME
            value: "{{ .Env.XNAT_DATASOURCE_USERNAME }}"
          - name: XNAT_DATASOURCE_PASSWORD
            value: "{{ .Env.XNAT_DATASOURCE_PASSWORD }}"
          - name: XNAT_HIBERNATE_DIALECT
            value: "{{ .Env.XNAT_HIBERNATE_DIALECT }}"
          - name: TOMCAT_XNAT_FOLDER
            value: "{{ .Env.XNAT_TOMCAT_FOLDER }}"
          - name: CATALINA_OPTS
            value: {{ .Env.XNAT_CATALINA_OPTS }}
          - name: PGPASSWORD
            value: "{{ .Env.XNAT_DATASOURCE_PASSWORD }}"
          - name: XNAT_API_URL
            value: "{{ .Env.XNAT_API_URL }}"
          - name: KEYCLOAK_HOST
            value: "{{ .Env.KEYCLOAK_BASE_URL }}"
          - name: KEYCLOAK_REALM
            value: "{{ .Env.KEYCLOAK_REALM }}"
          - name: KEYCLOAK_AUTH_ENDPOINT
            value: "{{ .Env.KEYCLOAK_AUTH_ENDPOINT }}"
          - name: KEYCLOAK_PUBLIC_CLIENT
            value: "{{ .Env.KEYCLOAK_PUBLIC_CLIENT }}"
          - name: XNAT_OPENID_ACCESS_TOKEN_BASE_URL
            value: "{{ .Env.XNAT_OPENID_ACCESS_TOKEN_BASE_URL }}"
          - name: XNAT_OPENID_USER_AUTH_BASE_URL
            value: "{{ .Env.XNAT_OPENID_USER_AUTH_BASE_URL }}"
          - name: TA_URL
            value: "{{ .Env.SSE_TA_BASE_URL }}"
          - name: SSE_URL
            value: "{{ .Env.SSE_SERVER_BASE_URL }}"
          - name: CP_ABE_URL
            value: "{{ .Env.CPABE_SERVER_BASE_URL }}"
          - name: SALT
            value: "{{ .Env.SSE_CLIENT_SALT }}"
          - name: IV
            value: "{{ .Env.SSE_CLIENT_IV }}"
          - name: ITER
            value: "{{ .Env.SSE_CLIENT_ITER }}"
          - name: KS
            value: "{{ .Env.SSE_CLIENT_KS }}"
          - name: TS
            value: "{{ .Env.SSE_CLIENT_TS }}"
          - name: HASH_LEN
            value: "{{ .Env.SSE_CLIENT_HASH_LEN }}"
          - name: CHUNK_SIZE
            value: "{{ .Env.SSE_CLIENT_CHUNK_SIZE }}"
          - name: NO_CHUNKS_PER_UPLOAD
            value: "{{ .Env.SSE_CLIENT_NO_CHUNKS_PER_UPLOAD }}"
          - name: SALT_TA
            value: "{{ .Env.SSE_CLIENT_SALT_TA }}"
          - name: IV_TA
            value: "{{ .Env.SSE_CLIENT_IV_TA }}"
          - name: ITER_TA
            value: "{{ .Env.SSE_CLIENT_ITER_TA }}"
          - name: KS_TA
            value: "{{ .Env.SSE_CLIENT_KS_TA }}"
          - name: TS_TA
            value: "{{ .Env.SSE_CLIENT_TS_TA }}"
          - name: SGX_ENABLE
            value: "{{ .Env.SSE_CLIENT_SGX_ENABLE }}"
          - name: DEBUG
            value: "{{ .Env.SSE_CLIENT_DEBUG }}"
          - name: AUTH
            value: "{{ .Env.SSE_CLIENT_AUTH }}"
          - name: SMALL_FILE
            value: "{{ .Env.SSE_CLIENT_SMALL_FILE }}"
        ports:
        - port: {{ .Env.INTERNAL_XNAT_PORT }}
        - containerPort: {{ .Env.INTERNAL_XNAT_PORT }}
      requirements:
      - host: application-server
      - volume:
          node: xnat-openid-provider-conf
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /data/xnat/home/config/auth/openid-provider.properties
              subPath: xnat-openid-provider.properties
    xnat-openid-provider-conf:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/conf

    sse-server:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: registry.gitlab.com/asclepios-project/symmetric-searchable-encryption-server:0.6
        env:
         - name: DJANGO_LOGLEVEL
           value: "{{ .Env.SSE_SERVER_DJANGO_LOGLEVEL }}"
         - name: DJANGO_DEBUG
           value: "{{ .Env.SSE_SERVER_DJANGO_DEBUG }}"
         - name: DJANGO_SECRET_KEY
           value: "{{ .Env.SSE_SERVER_DJANGO_SECRET_KEY }}"
         - name: ALLOWED_HOSTS
           value: "{{ .Env.ZUUL_HOST }} {{ .Env.INTERNAL_SSE_SERVER_HOST }}"
         - name: DB_NAME
           value: "{{ .Env.SSE_DB_NAME }}"
         - name: DB_USER
           value: "{{ .Env.SSE_DB_USER }}"
         - name: DB_PASSWORD
           value: "{{ .Env.SSE_DB_PASSWORD }}"
         - name: DB_HOST
           value: "{{ .Env.INTERNAL_SSE_DB_HOST }}"
         - name: DB_PORT
           value: "{{ .Env.INTERNAL_SSE_DB_PORT }}"
         - name: TA_SERVER
           value: "{{ .Env.INTERNAL_SSE_TA_BASE_URL }}"
         - name: MINIO_URL
           value: "{{ .Env.MINIO_HOST }}"
         - name: MINIO_ACCESS_KEY
           value: "{{ .Env.MINIO_ROOT_USER }}"
         - name: MINIO_SECRET_KEY
           value: "{{ .Env.MINIO_ROOT_PASSWORD }}"
         - name: MINIO_BUCKET_NAME
           value: "{{ .Env.SSE_SERVER_MINIO_BUCKET_NAME }}"
         - name: MINIO_SSL_SECURE
           value: "{{ .Env.SSE_SERVER_MINIO_SSL_SECURE }}"
         - name: MINIO_EXPIRE_GET
           value: "{{ .Env.SSE_SERVER_MINIO_EXPIRE_GET }}"
         - name: MINIO_EXPIRE_PUT
           value: "{{ .Env.SSE_SERVER_MINIO_EXPIRE_PUT }}"
        ports:
        - port: {{ .Env.INTERNAL_SSE_SERVER_PORT }}
        - containerPort: {{ .Env.INTERNAL_SSE_SERVER_PORT }}
      requirements:
      - host: application-server

    sse-ta:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: registry.gitlab.com/asclepios-project/sseta:0.5
        env:
         - name: DJANGO_LOGLEVEL
           value: "{{ .Env.SSE_TA_DJANGO_LOGLEVEL }}"
         - name: DJANGO_DEBUG
           value: "{{ .Env.SSE_TA_DJANGO_DEBUG }}"
         - name: DJANGO_SECRET_KEY
           value: "{{ .Env.SSE_TA_DJANGO_SECRET_KEY }}"
         - name: ALLOWED_HOSTS
           value: "{{ .Env.ZUUL_HOST }} {{ .Env.INTERNAL_SSE_TA_HOST }}"
         - name: DB_NAME
           value: "{{ .Env.SSE_TA_DB_NAME }}"
         - name: DB_USER
           value: "{{ .Env.SSE_TA_DB_USER }}"
         - name: DB_PASSWORD
           value: "{{ .Env.SSE_TA_DB_PASSWORD }}"
         - name: DB_HOST
           value: "{{ .Env.INTERNAL_SSE_TA_DB_HOST }}"
         - name: DB_PORT
           value: "{{ .Env.INTERNAL_SSE_TA_DB_PORT }}"
         - name: HASH_LENGTH
           value: "{{ .Env.SSE_TA_HASH_LENGTH }}"
         - name: IV
           value: "{{ .Env.SSE_TA_IV }}"
         - name: MODE
           value: "{{ .Env.SSE_TA_MODE }}"
         - name: KS
           value: "{{ .Env.SSE_TA_KS }}"
         - name: TEEP_SERVER
           value: "{{ .Env.INTERNAL_TEEP_SERVER_ENDPOINT }}"
         - name: SGX
           value: "{{ .Env.SSE_TA_SGX }}"
        ports:
        - port: {{ .Env.INTERNAL_SSE_TA_PORT }}
        - containerPort: {{ .Env.INTERNAL_SSE_TA_PORT }}
      requirements:
      - host: application-server

    abac-zuul-proxy:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: registry.gitlab.com/asclepios-project/abac-authorization/abac-zuul-proxy:3.0.3
        env:
         - name: SSE_URL
           value: "{{ .Env.INTERNAL_SSE_SERVER_BASE_URL }}"
         - name: SSE_TA_URL
           value: "{{ .Env.INTERNAL_SSE_TA_BASE_URL }}"
         - name: PDP_ENDPOINTS
           value: "{{ .Env.INTERNAL_ABAC_SERVER_PDP_ENDPOINT }}"
         - name: PDP_ACCESS_KEY
           value: "{{ .Env.ABAC_SERVER_ACCESS_KEY }}"
         - name: PDP_TRUSTSTORE_FILE
           value: "{{ .Env.COMMON_TRUSTSTORE_FILE }}"
         - name: PDP_TRUSTSTORE_PASSWORD
           value: "{{ .Env.COMMON_TRUSTSTORE_PASSWORD }}"
         - name: PDP_JWT_SECRET
           value: "{{ .Env.ABAC_ZUUL_PROXY_PDP_JWT_SECRET }}"
         - name: PDP_LOAD_BALANCE_METHOD
           value: "{{ .Env.ABAC_ZUUL_PROXY_PDP_LOAD_BALANCE_METHOD }}"
         - name: PDP_RETRY_COUNT
           value: "{{ .Env.ABAC_ZUUL_PROXY_PDP_RETRY_COUNT }}"
         - name: KEYCLOAK_ENABLED
           value: "{{ .Env.ABAC_ZUUL_PROXY_KEYCLOAK_ENABLED }}"
         - name: KEYCLOAK_URL
           value: "{{ .Env.KEYCLOAK_AUTH_ENDPOINT }}"
         - name: KEYCLOAK_REALM
           value: "{{ .Env.KEYCLOAK_REALM }}"
         - name: KEYCLOAK_RESOURCE
           value: "{{ .Env.KEYCLOAK_PUBLIC_CLIENT }}"
        ports:
        - port: {{ .Env.INTERNAL_ZUUL_PORT }}
        - containerPort: {{ .Env.INTERNAL_ZUUL_PORT }}
      requirements:
      - host: application-server
      - volume:
          node: abac-zuul-proxy-config
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /abac-proxy/config/application.yml
              subPath: zuul.application.yml
      - volume:
          node: abac-zuul-proxy-auth-client-props
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /abac-proxy/config/authorization-client.properties
              subPath: zuul.authorization-client.properties
      - volume:
          node: abac-zuul-proxy-truststore
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /etc/certs/common-truststore.p12
              subPath: common-truststore.p12
    abac-zuul-proxy-truststore:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/certs/common
    abac-zuul-proxy-config:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/conf
    abac-zuul-proxy-auth-client-props:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/conf

    abac-server:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: docker.gitlab.gwdg.de/snet-asclepios-demo/dockerfiles/somnonetz/snet-abac-server:50f7fffad0a4776ccb8d021dd8c8d4e4fcb147ce
        command:
        - sh
        args:
        - -c
        - "./run.sh"
        env:
         - name: ABAC_SERVER_KEYSTORE_FILE
           value: "{{ .Env.ABAC_SERVER_KEYSTORE_FILE }}"
         - name: ABAC_SERVER_KEY_ALIAS
           value: "{{ .Env.ABAC_SERVER_KEY_ALIAS }}"
         - name: ABAC_SERVER_KEYSTORE_PASSWORD
           value: "{{ .Env.ABAC_SERVER_KEYSTORE_PASSWORD }}"
         - name: ABAC_SERVER_TRUSTSTORE_FILE
           value: "{{ .Env.COMMON_TRUSTSTORE_FILE }}"
         - name: ABAC_SERVER_TRUSTSTORE_PASSWORD
           value: "{{ .Env.COMMON_TRUSTSTORE_PASSWORD }}"
         - name: ABAC_SERVER_JPA_HIBERNATE_DDL_AUTO
           value: "{{ .Env.ABAC_SERVER_JPA_HIBERNATE_DDL_AUTO }}"
         - name: ABAC_SERVER_ACCESS_KEY
           value: "{{ .Env.ABAC_SERVER_ACCESS_KEY }}"
         - name: ABAC_SERVER_API_KEY
           value: "{{ .Env.ABAC_SERVER_API_KEY }}"
         - name: ABAC_SERVER_PORT
           value: "{{ .Env.INTERNAL_ABAC_SERVER_PORT }}"
         - name: XNAT_API_URL
           value: "{{ .Env.XNAT_API_URL }}"
         - name: KEYTRAY_HOST
           value: "{{ .Env.KEYTRAY_HOST }}"
         - name: SSE_HOST
           value: "{{ .Env.ABAC_SERVER_SSE_HOST }}"
         - name: XNAT_USER
           value: "{{ .Env.XNAT_ADMIN_USER }}"
         - name: XNAT_PASSWORD
           value: "{{ .Env.XNAT_ADMIN_PASSWORD }}"
        ports:
        - port: {{ .Env.INTERNAL_ABAC_SERVER_PORT }}
        - containerPort: {{ .Env.INTERNAL_ABAC_SERVER_PORT }}
      requirements:
      - host: application-server
      - volume:
          node: abac-server-policies-dir
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /abac-server/policies
      - volume:
          node: abac-server-truststore
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /etc/certs/common-truststore.p12
              subPath: common-truststore.p12
      - volume:
          node: abac-server-keystore
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /etc/certs/abac-server-keystore.p12
              subPath: abac-server-keystore.p12
    abac-server-truststore:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/certs/common
    abac-server-keystore:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/certs/abac-server
    abac-server-policies-dir:
      type: tosca.nodes.MiCADO.Container.Volume.NFS
      properties:
        server: 10.255.230.92
        path: /share/policies

    # Persistance node

    xnat-db:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: postgres:12.7-alpine
        env:
          - name: POSTGRES_USER
            value: "{{ .Env.XNAT_DATASOURCE_USERNAME }}"
          - name: POSTGRES_PASSWORD
            value: "{{ .Env.XNAT_DATASOURCE_PASSWORD }}"
          - name: POSTGRES_DB
            value: "{{ .Env.XNAT_DATASOURCE_DBNAME }}"
        ports:
        - port: {{ .Env.INTERNAL_XNAT_DB_PORT }}
      requirements:
      - volume:
          node: xnat-db-dir
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /var/lib/postgresql/data
      - volume:
          node: xnat-db-sql-entrypoint
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /docker-entrypoint-initdb.d/
    xnat-db-dir:
      type: tosca.nodes.MiCADO.Container.Volume.Local
      properties:
        node: sleep-persistence
        path: /srv/data/xnat-db
    xnat-db-sql-entrypoint:
      type: tosca.nodes.MiCADO.Container.Volume.Local
      properties:
        node: sleep-persistence
        path: /srv/data/xnat-db-docker-entrypoint-initdb.d/

    sse-db:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: postgres:12
        env:
         - name: POSTGRES_USER
           value: "{{ .Env.SSE_DB_USER }}"
         - name: POSTGRES_PASSWORD
           value: "{{ .Env.SSE_DB_PASSWORD }}"
         - name: POSTGRES_DB
           value: "{{ .Env.SSE_DB_NAME }}"
        ports:
        - port: {{ .Env.INTERNAL_SSE_DB_PORT }}
      requirements:
      - volume:
          node: sse-db-dir
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /var/lib/postgresql/data
    sse-db-dir:
      type: tosca.nodes.MiCADO.Container.Volume.Local
      properties:
        node: sleep-persistence
        path: /srv/data/sse-db

    sse-ta-db:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: postgres:12
        env:
         - name: POSTGRES_USER
           value: "{{ .Env.SSE_TA_DB_USER }}"
         - name: POSTGRES_PASSWORD
           value: "{{ .Env.SSE_TA_DB_PASSWORD }}"
         - name: POSTGRES_DB
           value: "{{ .Env.SSE_TA_DB_NAME }}"
        ports:
        - port: {{ .Env.INTERNAL_SSE_TA_DB_PORT }}
      requirements:
      - volume:
          node: sse-ta-db-dir
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /var/lib/postgresql/data
    sse-ta-db-dir:
      type: tosca.nodes.MiCADO.Container.Volume.Local
      properties:
        node: sleep-persistence
        path: /srv/data/sse-ta-db

    minio-s3-server:
      type: tosca.nodes.MiCADO.Container.Application.Docker.Deployment
      properties:
        image: minio/minio
        command:
           - /bin/bash
           - -c
           - mkdir -p /data/snet && minio server /data
        env:
         - name: MINIO_ROOT_USER
           value: "{{ .Env.MINIO_ROOT_USER }}"
         - name: MINIO_ROOT_PASSWORD
           value: "{{ .Env.MINIO_ROOT_PASSWORD }}"
        ports:
        - port: {{ .Env.INTERNAL_MINIO_PORT }}
        - containerPort: {{ .Env.INTERNAL_MINIO_PORT }}
      requirements:
      - volume:
          node: minio-dir
          relationship:
            type: tosca.relationships.AttachesTo
            properties:
              location: /data
    minio-dir:
      type: tosca.nodes.MiCADO.Container.Volume.Local
      properties:
        node: sleep-persistence
        path: /srv/data/minio
