# snet-asclepios-deployment

Encrypted sleep research platform. This repositiory contains tools and definitions for provisioning and deploying the ASCLEPIOS project sleep healthcare demonstrator.

The deployed stack includes [XNAT](https://xnat.org/) with [snet-asclepios-plugin](https://gitlab.com/indie-sleep-demo/snet-asclepios-plugin), [snet-asclepios-editor](https://gitlab.com/indie-sleep-demo/snet-asclepios-editor) and [snet-asclepios-search](https://gitlab.com/indie-sleep-demo/snet-asclepios-search) along with a number of services from the ASCLEPIOS framework.

### Development and Deployment Dependencies

These dependencies (and others) can be installed by the `configure` script (see next section):

- `docker` and `docker-compose`
- optional: You may want skip the host depenency installation stage of the config script, and install any requirements using an alternative method. To do this, run this before proceeding to the next section: 

```
touch .configure.nohostdeps
```
- avoiding host dependency installation should allow you run run the `./configure` script as a non-root user 

### Running everything on a single node (for development)
1. Add the following to your local `/etc/hosts` file:

```
127.0.0.1 snet-apps.local
127.0.0.1 keycloak.snet-apps.local
127.0.0.1 registration-authority.snet-apps.local
127.0.0.1 keytray.snet-apps.local
127.0.0.1 zuul.snet-apps.local
127.0.0.1 xnat.snet-apps.local

127.0.0.1 minio
```

1. Run `export APP_HOST_NAME="snet-apps.local"`.
Note: export this to the root user's environment because the script in the next step requires root privileges. Alternatively, you can create the directory `.env.d` and put the value into the file `.env.d/APP_HOST_NAME`.
1. Run `sudo ./configure` and follow output until you get a green success message.
1. Run `docker network create snet-asclepios`
1. Run `./bin/dcall up -d` to bring up the whole stack.

### Running one of the modularized sets of services

1. Run `./bin/dcf docker-compose/<file>.yml up -d`

### SSL

1. optional: may want to enable TLS and get a SSL certificates for any applications that you are running on a public host

For this to work you will need to be running on a host which has port 80 and port 443 available on the public internet (for certbot)

```
mv docker-compose/notls-gateway.yml docker-compose/notls-gateway.yml.off
mv docker-compose/tls-gateway.yml.off mv docker-compose/tls-gateway.yml
docker network create snet-asclepios
./bin/dcf docker-compose/<file>.yml up -d
./bin/dcf docker-compose/docker-compose/tls-gateway up -d
```

### Set up Keycloak

You need to import `conf/realm-export.json` in order to provision the keycloak realm, but before you can do that you need to make a minor change the keycloak database schema, specifically, you need to modify the `USER_ATTRIBUTE.text` column to allow the storage of strings greater than 255 characters.

The change is necessary to faciliate the storage of cpabe keys as user attributes, as a cpabe key is usually more than 1000 characters in length.

- Execute the following command as root:

```sh
docker exec --env-file .env keycloak-db mysql --user="root" --password="$(cat .env.d/KEYCLOAK_DB_ROOT_PASSWORD)" --database=keycloak --execute='ALTER TABLE USER_ATTRIBUTE MODIFY value varchar(2000) CHARACTER SET utf8 COLLATE utf8_general_ci;
```

Now you can import the realm manually using the keycloak admin UI

* Open the Keycloak admin interface in a web browser.
- Log in with the credential output by `grep KEYCLOAK_ADMIN .env` to show the credentials for keycloak.
- Click `Add Realm` at the top left under `Master`
- Click `Import -> Select File` and select the file at `./conf/realm-export.json` in this repository

Next you will need to create a realm admin user so that you can use the Registration Authority

- Identify the admin user name as the value of `KEYCLOAK_REALM_ADMIN_USER` in `.env`
- Go to `Users->Add User` and create a new user with that exact name. You can click `Save` already.
- Run `cat .env.d/KEYCLOAK_REALM_ADMIN_PASSWORD` to retrieve the user's password.
- On the user screen go to `Credentials` and set the password to the value by output the previous command. Click `Save Password` now.
- On the user screen go to `Role Mappings` and assign the `admin` realm role by selected `Admin` from the `Available Roles` and clicking `Add selected`.

Optionally, you can provision the realm admin with a CPABE key using the following script:

```
python3 -m utils.init_realm_admin
usage: init_realm_admin.py [-h] username project
init_realm_admin.py: error: the following arguments are required: username, project
```

Finally, if you want to run the automated end-to-end tests, you need to set a password for the test runner.

- Run `grep TEST_KEYCLOAK .env` to view the user name and password for the test runner user.
- Go to `Users->View All Users` in the sidebar, and then for test runner user go to the `Credentials` tab and set the password to the value output by the previous command.


### Set up XNAT

XNAT's database is provisioned using a SQL dump. This SQL dump contains the default XNAT admin user, with the password 'admin' (just like on a default xnat installation).

* Open the XNAT admin interface in a web browser.
* Login with the credentials `username=admin,password=admin`
- Run `cat .env.d/XNAT_ADMIN_PASSWORD`
* Go to `Adminster -> Users -> admin -> Change Password` and set the password to value output by the previous command.

You can now use the admin user to create new projects and activtate new user accounts.

When creating a new project, you must add an ASCLEPIOS `keyid` field for subjects in the project. See `Manage Custom Variables` in the `asclepiostestproject` fixture for an example of how to do this.

### Adding new users

New users can be added to keycloak via the Registration Authority using the following utlity script. `role` should be one of `owner`, `member` or `collaborator`

```
$ python3 -m utils.init_realm_admin
usage: init_realm_admin.py [-h] username firstName lastName email project
```

* Once a user has been created, set there password to a tempory password in the keycloak admin interface.
* They can now attempt to log into XNAT using the `login with keycloak` button, they should get a registration successful message.
* An administrator must use the XNAT admin user to manually activate their account, and add them to the correct project.

### Adding a new data subject

* Provision a new set of subject keys and add a new subject to XNAT using the following utility script

```
python3 -m utils.create_subject
usage: create_subject.py [-h] label project
create_subject.py: error: the following arguments are required: label, project
```

### Browser Applications

You should now be able to access the following applications and login with keycloak:

* `snet-asclepios-editor` at `xnat.snet-apps.local/sn-editor
* `snet-asclepios-search` at `xnat.snet-apps.local/asclepios-search`

### Run tests

* There are some end-to-end style API tests in the `utils/e2e_tests` directory.
* Make sure you have set a password for the test runner (see "Set up Keycloak" above)
* You can run them with the following command:

```
./configure
python3 -m unittest utils.e2e_tests
```

### Continuous Deployment
- use GitLab
- enable CI/CD
- generate an RSA(!) SSH keypair locally
- under Project -> ``Settings -> CI/CD -> General Pipelines`:
  - use `git clone`
  - set shallow clone depth to `0`
  - add the SSH private key as `SSH_PRIVATE_KEY` to CI variables
- on target machine:
  - add the SSH public key to the user's `~/.ssh/authorized_keys`
  - allow passwordless sudo
  - create remote directory
  - run `git init` and `git config receive.denyCurrentBranch updateInstead
` from that directory
- in the `.gitlab-ci.yml`
  - add a job that `extends` `.deploy-template`
  - add all the necessary environment variables to the job, e.g. `SSH_UPLOAD_DIR`. The variables are listed in the template job's description.
  - consider adding an `only` keyword to restrict deployments, e.g. to the `master` branch
- make sure the runner can contact the target machine on TCP port 22
