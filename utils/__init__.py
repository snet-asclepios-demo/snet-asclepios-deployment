from types import SimpleNamespace
from os import environ
from dotenv import load_dotenv

load_dotenv('.env')
env = SimpleNamespace(**dict(environ))
