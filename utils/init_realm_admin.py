from secrets import token_hex

from . import env

from .lib.keycloak import get_keycloak_access_token
from .lib.registration_authority import add_user_with_cpabe_key, edit_user_and_regen_cpabe_key

keycloak_env = [env.KEYCLOAK_AUTH_ENDPOINT, env.KEYCLOAK_REALM, env.KEYCLOAK_PUBLIC_CLIENT, env.KEYCLOAK_REALM_ADMIN_USER, env.KEYCLOAK_REALM_ADMIN_PASSWORD]


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("username", help="Keycloak Username", type=str)
    parser.add_argument("firstName", help="First Name", type=str)
    parser.add_argument("lastName", help="Last Name", type=str)
    parser.add_argument("email", help="Email Address", type=str)
    parser.add_argument("project", help="Project", type=str)
    args = parser.parse_args()

    payload = {
        "username":  args.username,
        "firstName": args.firstName,
        "lastName": args.lastName,
        "email": args.email,
        "keys": ["project", "role"],
        "values": [args.project, "owner"],
        "enabled": "true",
        "realmRoles": ['admin'],
    }

    access_token = get_keycloak_access_token(*keycloak_env)
    res = edit_user_and_regen_cpabe_key(env.REGISTRATION_AUTHORITY_BASE_URL, args.username, payload, access_token)

    data = res.read()

    print('Registration Authority response:', res.status, data.decode('ascii'))

