from .test_000_keycloak import *
from .test_001_xnat import *
from .test_002_registration_authority import *
from .test_003_cpabe_server import *
from .test_004_keytray import *
from .test_005_sse import *


if __name__ == "__main__":
    unittest.main()