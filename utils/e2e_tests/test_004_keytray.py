import unittest

from .. import env

from ..lib.http import request
from ..lib.keycloak import get_keycloak_access_token

keycloak_env = [env.KEYCLOAK_AUTH_ENDPOINT, env.KEYCLOAK_REALM, env.KEYCLOAK_PUBLIC_CLIENT, env.TEST_KEYCLOAK_USER, env.TEST_KEYCLOAK_PASSWORD]


# class TestKeyTray(unittest.TestCase):
#     def setUp(self):
#         self.access_token = get_keycloak_access_token(*keycloak_env)
#
#     def test_health(self):
#         headers = {"Authorization": "Bearer " + self.access_token}
#
#         res =request(env.KEYTRAY_BASE_URL + "/", headers=headers)
#
#         assert (res.status == 200)
