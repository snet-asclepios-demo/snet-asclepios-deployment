import unittest
import json
from uuid import uuid4

from .. import env

from ..lib.keycloak import get_keycloak_access_token
from ..lib.registration_authority import add_user_with_cpabe_key, get_users, edit_user_and_regen_cpabe_key

keycloak_env = [env.KEYCLOAK_AUTH_ENDPOINT, env.KEYCLOAK_REALM, env.KEYCLOAK_PUBLIC_CLIENT, env.TEST_KEYCLOAK_USER, env.TEST_KEYCLOAK_PASSWORD]


class TestRegistrationAuthority(unittest.TestCase):
    def setUp(self):
        self.access_token = get_keycloak_access_token(*keycloak_env)

    @classmethod
    def setUpClass(cls):
        cls.username = str(uuid4()).replace('-', '')

    def test_000_add_user_with_cpabe_key(self):
        """
        Test Registration Authority create user endpoint

        Passes iff:
            * Registration Authority returned an 'OK' status
        """

        payload = {
            "firstName":"test",
            "lastName":"artifact",
            "email":f"{self.username}@example.com",
            "username": self.username,
            "enabled":"true",
            "keys":["project","role"],
            "values":[env.TEST_PROJECT, "testing"],
            "realmRoles":[],
        }

        res = add_user_with_cpabe_key(env.REGISTRATION_AUTHORITY_BASE_URL, payload, self.access_token)

        assert(res.status == 200) # upstream bug: should change this to a 201

        data = res.read()

        assert(data != b'Added user with idnull') # upstream bug: returns a (broken) success message even if something went wrong



    def test_002_get_users(self):
        """
        Test Registration Authority get users endpoint

        Passes iff:
            * Registration Authority returned an 'OK' status
            * The previously created user is in the output
            * The previously created has a cpabe key of non-zero length
        """

        res = get_users(env.REGISTRATION_AUTHORITY_BASE_URL, self.access_token)

        assert (res.status == 200)

        data = json.load(res)

        user = next((u for u in data if u['username'] == self.username), None)

        assert(user)
        assert('cpabe' in user['attributes'])
        assert(len(user['attributes']['cpabe']) > 0)

    def test_003_edit_user(self):
        """
        Test Registration Authority edit user endpoint

        Passes iff:
            * Registration Authority returned an 'OK' status
        """

        payload = {
            "firstName":"test",
            "lastName":"artifact",
            "email":f"{self.username}@example.com",
            "username": self.username,
            "enabled":"true",
            "keys":["project","role","foo"],
            "values":[env.TEST_PROJECT, "testing","bar"],
            "realmRoles":[],
        }

        res = edit_user_and_regen_cpabe_key(env.REGISTRATION_AUTHORITY_BASE_URL, self.username, payload, self.access_token)

        assert(res.status == 200)
