import unittest
import http.client
import json
from uuid import uuid4

from .. import env

from ..lib.http import encode_basic_auth_credentials
from ..lib.xnat import do_xnat_api_authentication, get_xnat_subjects, create_xnat_subject, key_id_field


class TestXNAT(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.subject_label = str(uuid4()).replace('-', '')
        cls.subject_key_id = str(uuid4())


    def test_000_api_auth(self):
        """
        Test XNAT api authentication

        Passes iff:
            * XNAT returned an 'OK' status
            * XNAT returned something that looks like a JSESSIONID
        """

        res = do_xnat_api_authentication(env.XNAT_API_URL, env.XNAT_ADMIN_USER, env.XNAT_ADMIN_PASSWORD)
        data = res.read()

        assert (res.status == 200)
        assert (len(data) == 32) # a JSESSIONID is a 32 character strings

    def test_001_create_subject(self):
        """
        Test XNAT create subject endpoint

        Passes iff:
            * XNAT returned an 'created' status
        """

        res = create_xnat_subject(env.XNAT_API_URL, env.XNAT_ADMIN_USER, env.XNAT_ADMIN_PASSWORD, env.TEST_PROJECT, self.subject_label, self.subject_key_id)

        assert (res.status == 201)

    def test_002_get_subjects(self):
        """
        Test XNAT get subjects endpoint

        Passes iff:
            * XNAT returned an 'OK' status
            * The previously created subject is in the output
            * The previously created subject has the correct key id
        """

        res = get_xnat_subjects(env.XNAT_API_URL, env.XNAT_ADMIN_USER, env.XNAT_ADMIN_PASSWORD, env.TEST_PROJECT)

        assert (res.status == 200)

        data = json.load(res)
        results = data['ResultSet']['Result']

        subject = next((s for s in results if s['label'] == self.subject_label), None)

        assert(subject)
        assert(subject[key_id_field] == self.subject_key_id)