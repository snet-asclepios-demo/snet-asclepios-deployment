import unittest

from .. import env

from ..lib.http import request
from ..lib.keycloak import get_keycloak_access_token

keycloak_env = [env.KEYCLOAK_AUTH_ENDPOINT, env.KEYCLOAK_REALM, env.KEYCLOAK_PUBLIC_CLIENT, env.TEST_KEYCLOAK_USER, env.TEST_KEYCLOAK_PASSWORD]


class TestSSE(unittest.TestCase):
    def setUp(self):
        self.access_token = get_keycloak_access_token(*keycloak_env)

    def test_sse_health(self):
        headers = {"Authorization": "Bearer " + self.access_token}

        res = request(env.SSE_SERVER_BASE_URL + "/api/v1/", headers=headers)

    def test_ta_health(self):
        headers = {"Authorization": "Bearer " + self.access_token}

        res =request(env.SSE_TA_BASE_URL + "/api/v1/", headers=headers)

        assert (res.status == 200)
