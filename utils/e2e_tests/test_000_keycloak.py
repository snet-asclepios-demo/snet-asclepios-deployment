import unittest
import json

from .. import env

from ..lib.keycloak import do_keycloak_api_authentication



class TestKeyCloak(unittest.TestCase):
    def test_000_api_authentication(self):
        """
        Check KeyCloak is online and working for authentication

        Passes iff:
            * KeyCloak returned an 'OK' status
            * KeyCloak returned a JSON response body containing an access_token
        """
        res = do_keycloak_api_authentication(
            env.KEYCLOAK_AUTH_ENDPOINT,
            env.KEYCLOAK_REALM,
            env.KEYCLOAK_PUBLIC_CLIENT,
            env.TEST_KEYCLOAK_USER,
            env.TEST_KEYCLOAK_PASSWORD
        )

        obj = json.loads(res.read())

        assert (res.status == 200)
        assert ('access_token' in obj)
