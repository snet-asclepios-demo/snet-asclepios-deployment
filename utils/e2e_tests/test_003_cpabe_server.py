
import unittest
import json
from os import urandom
from binascii import b2a_hex
from uuid import uuid4

from .. import env

from ..lib.keycloak import get_keycloak_access_token
from ..lib.registration_authority import edit_user_and_regen_cpabe_key
from ..lib.cpabe_server import set_cpabe_policy, get_current_cpabe_policy, encrypt_and_upload_sse_keys, download_and_decrypt_sse_keys
from ..lib.xnat import create_xnat_subject

keycloak_env = [env.KEYCLOAK_AUTH_ENDPOINT, env.KEYCLOAK_REALM, env.KEYCLOAK_PUBLIC_CLIENT, env.TEST_KEYCLOAK_USER, env.TEST_KEYCLOAK_PASSWORD]

# for storing keyid returned by encrypt_and_upload_sse_keys
state = {}


class TestCPABEServer(unittest.TestCase):
    def setUp(self):
        self.access_token = get_keycloak_access_token(*keycloak_env)

    @classmethod
    def setUpClass(cls):
        cls.policy = f"role:testing role:owner 1of2 project:{env.TEST_PROJECT} 2of2"
        cls.ver_key = b2a_hex(urandom(16)).decode('ascii')
        cls.enc_key = b2a_hex(urandom(32)).decode('ascii')

        # the testrunner user needs a CPABE key for the following tests
        payload = {
            "firstName":"test",
            "lastName":"artifact",
            "email":f"{env.TEST_KEYCLOAK_USER}@example.com",
            "username": env.TEST_KEYCLOAK_USER,
            "enabled":"true",
            "keys":["project","role"],
            "values":[env.TEST_PROJECT, "testing"],
            "realmRoles":[],
        }

        edit_user_and_regen_cpabe_key(env.REGISTRATION_AUTHORITY_BASE_URL, env.TEST_KEYCLOAK_USER, payload, get_keycloak_access_token(*keycloak_env))

    def test_000_set_cpabe_policy(self):
        """
        Test CPABE Server set current policy

        Passes iff:
            * CPABE Server returned an 'OK' status
        """
        res = set_cpabe_policy(env.CPABE_SERVER_BASE_URL, self.policy, self.access_token)

        assert (res.status == 200)

    def test_001_get_current_cpabe_policy(self):
        """
        Test CPABE Server get current policy endpoint

        Passes iff:
            * CPABE Server returned an 'OK' status
            * The previously set policy is in the output
        """
        res = get_current_cpabe_policy(env.CPABE_SERVER_BASE_URL, self.access_token)

        assert (res.status == 200)

        output = res.read()

        expected = (b'The current enforced ABE Policy is ' + self.policy.encode('ascii'))

        assert (expected == output)

    def test_002_encrypt_and_upload_sse_keys(self):
        """
        Test CPABE Server upload and encrypted sse keys endpoint

        Passes iff:
            * CPABE Server returned an 'OK' status
            * The output is a key_id (uuid) wrapped in quotes
            * A new subject with that key_id can be added to XNAT
        """
        res = encrypt_and_upload_sse_keys(env.CPABE_SERVER_BASE_URL, self.ver_key, self.enc_key, self.access_token)

        assert (res.status == 200)

        data = res.read()

        assert (len(data) == 38) # 32 chars of hex + 2 quotes + 4 hyphens

        # save key_id for next test
        state['key_id'] = data.decode('ascii').replace('"', '')

        # add a new subject with that key_id on xnat
        # this is needed because the next test uses xnat as attribute source for
        # access control

        res = create_xnat_subject(
            env.XNAT_API_URL,
            env.XNAT_ADMIN_USER,
            env.XNAT_ADMIN_PASSWORD,
            env.TEST_PROJECT,
            uuid4(),
            state['key_id']
        )

        assert (res.status == 201)

    def test_003_download_and_decrypt_sse_keys(self):
        """
        Test CPABE Server download an decrypt sse keys

        Passes iff:
            * CPABE Server returned an 'OK' status
            * The previously created keys are in the output
        """
        res = download_and_decrypt_sse_keys(env.CPABE_SERVER_BASE_URL, state['key_id'], self.access_token)

        assert (res.status == 200)

        data = json.load(res)

        assert (data['verKey'] == self.ver_key)
        assert (data['encKey'] == self.enc_key)
