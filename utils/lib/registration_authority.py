import json

from .http import request


def add_user_with_cpabe_key(registration_authority_url, payload, access_token=""):
    """
    Use the Registration Authority to create a KeyCloak user and provision it
    with a CPABE key

    Payload has the following format:

    {
        "firstName": string,
        "lastName": string,
        "email": string,
        "username": string,
        "enabled": "true|false",
        "keys": [string, ...],
        "values": [string, ...],
        "realmRoles": [string, ...],
    }

    Where keys are is a list of attributes names and values is the list of
    corresponding values
    """

    url = registration_authority_url + "/api/v1/auth/add-user"

    headers = {
        'Content-Type': "application/json",
        "Authorization": "Bearer " + access_token
    }

    data = json.dumps(payload).encode('ascii')

    return request(url, data, headers)


def edit_user_and_regen_cpabe_key(registration_authority_url, username, payload, access_token=""):
    """
    Use the Registration Authority to edit a KeyCloak user and regenerate it's
    with a CPABE key

    Payload has the following format:

    {
        "firstName": string,
        "lastName": string,
        "email": string,
        "username": string,
        "enabled": "true|false",
        "keys": [string, ...],
        "values": [string, ...],
        "realmRoles": [string, ...],
    }

    Where keys are is a list of attributes names and values is the list of
    corresponding values
    """

    url = registration_authority_url + f"/api/v1/auth/edit-user/{username}"

    headers = {
        'Content-Type': "application/json",
        "Authorization": "Bearer " + access_token
    }

    data = json.dumps(payload).encode('ascii')

    return request(url, data, headers)


def get_users(registration_authority_url, access_token=""):
    """
    Get a list of KeyCloak users from the Registration Authority
    """

    headers = {
        'Content-Type': "application/json",
        "Authorization": "Bearer " + access_token
    }

    url = registration_authority_url + "/api/v1/auth/getusers"

    return request(url, headers=headers)