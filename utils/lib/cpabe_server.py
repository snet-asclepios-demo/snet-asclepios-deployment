import json

from .http import request


def get_current_cpabe_policy(cpabe_server_url, access_token=""):
    """
    Get the current encryption policy from the CPABE server
    """

    headers = {
        "Authorization": "Bearer " + access_token
    }
    url = cpabe_server_url + '/api/v1/currentpolicy'

    return request(url, headers=headers)


def set_cpabe_policy(cpabe_server_url, policy, access_token=""):
    """
    Set a new policy for encryption on the CPABE server.
    example policy:

    "role:member role:owner role:collaborator 1of3 project:sleep-demo-study 2of2 userid:some-id 1of2"

    which is equivelent to the following logic:

    (userid = someid) OR (
        (role = member OR role = owner OR role = collaborator) AND project = sleep-demo-study)
    """

    url = cpabe_server_url + '/api/v1/setpolicy'

    headers = {
        'Content-Type': 'application/octet-stream',
        "Authorization": "Bearer " + access_token
    }

    return request(url, policy.encode('ascii'), headers)


def encrypt_and_upload_sse_keys(cpabe_server_url, ver_key, enc_key, access_token=""):
    """
    Encrypt a pair of SSE keys with the currently set policy and store it in the KeyTray
    Return a key_id
    """

    url = cpabe_server_url + '/api/v1/put'

    data = json.dumps({"verKey": ver_key, "encKey": enc_key}).encode('ascii')

    headers = {
        'Content-Type': "application/json",
        "Authorization": "Bearer " + access_token
    }

    return request(url, data, headers)


def download_and_decrypt_sse_keys(cpabe_server_url, key_id, access_token=""):
    """
    Download encrypted SSE key pair from KeyTray and decrypt with current users CPABE key
    """

    url = cpabe_server_url + '/api/v1/get'

    data = json.dumps({"uuid": key_id}).encode('ascii')

    headers = {
        'Content-Type': "application/json",
        "Authorization": "Bearer " + access_token
    }

    return request(url, data, headers)
