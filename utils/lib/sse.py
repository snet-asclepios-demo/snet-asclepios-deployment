import json

from .http import request


def upload_ver_key(ta_url, ver_key, key_id, access_token=""):
    """
    Upload a verification key to the Trusted Authority
    """

    url = ta_url + '/api/v1/key/'

    data = json.dumps({"key": ver_key, "keyId": key_id}).encode('ascii')

    headers = {
        'Content-Type': 'application/json',
        "Authorization": "Bearer " + access_token
    }

    return request(url, data, headers)
