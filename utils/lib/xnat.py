from urllib.parse import urlencode, quote_plus

from .http import request, encode_basic_auth_credentials

key_id_field = 'xnat:subjectdata/fields/field[name=keyid]/field'


def do_xnat_api_authentication(xnat_api_url, username, password):
    """
    Authenticate with XNAT and return a JSESSIONID
    """

    url = xnat_api_url + "/services/auth"

    data = urlencode({
        "username": username,
        "password": password,
    }).encode('ascii')

    headers = {'Content-Type': "application/x-www-form-urlencoded"}

    return request(url, data, headers, 'PUT')


def get_xnat_subjects(xnat_api_url, username, password, project):
    """
    Get an all XNAT subjects in a project, including ASCLEPIOS keyid in the output
    """

    creds = encode_basic_auth_credentials(username, password)
    columns = quote_plus(f"label,{key_id_field}")

    url = xnat_api_url + f"/projects/{project}/subjects?columns={columns}&format=json"

    headers = { 'Authorization' : f"Basic {creds}" }

    return request(url, headers=headers)


def create_xnat_subject(xnat_api_url, username, password, project, subject_name, key_id):
    """
    Create an XNAT subject, populating the ASCLEPIOS keyid field
    """

    creds = encode_basic_auth_credentials(username, password)
    key_id_param = quote_plus(key_id_field)

    headers = { 'Authorization' : f"Basic {creds}" }

    url = xnat_api_url + f"/projects/{project}/subjects/{subject_name}?{key_id_param}={key_id}"

    return request(url, headers=headers, method='PUT')