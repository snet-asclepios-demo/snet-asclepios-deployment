import json
from functools import wraps
from urllib.parse import urlencode

from .http import request


def do_keycloak_api_authentication(KEYCLOAK_AUTH_ENDPOINT, realm, client_id, username, password):
    """
    Authenticate with KeyCloak and return an access token
    """

    url = KEYCLOAK_AUTH_ENDPOINT + f"/realms/{realm}/protocol/openid-connect/token"

    data = urlencode({
        "client_id": client_id,
        "grant_type": "password",
        "username": username,
        "password": password,
    }).encode('ascii')

    headers = { 'Content-Type': "application/x-www-form-urlencoded" }

    return request(url, data, headers)


def get_keycloak_access_token(KEYCLOAK_AUTH_ENDPOINT, realm, client_id, username, password):
    res = do_keycloak_api_authentication(KEYCLOAK_AUTH_ENDPOINT, realm, client_id, username, password)
    data = json.load(res)
    return data['access_token']
