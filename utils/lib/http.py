import json
from base64 import b64encode
from urllib.request import Request, urlopen
from urllib.error import HTTPError


def request(url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None):
    req = Request(url, data, headers, origin_req_host, unverifiable, method)
    response = urlopen(req)
    return response


def encode_basic_auth_credentials(username, password):
    return b64encode(f"{username}:{password}".encode("utf-8")).decode("ascii")