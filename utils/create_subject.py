from secrets import token_hex

from . import env

from .lib.keycloak import get_keycloak_access_token
from .lib.xnat import create_xnat_subject
from .lib.cpabe_server import set_cpabe_policy, encrypt_and_upload_sse_keys
from .lib.sse import upload_ver_key


def gen_keys():
    print('Generating SSE key pair')

    ver_key = token_hex(16)
    enc_key = token_hex(32)

    print("ver_key:", ver_key)
    print("enc_key:", enc_key)

    return ver_key, enc_key


def set_policy(project, access_token):
    print('Setting CPABE policy')

    policy = f"role:owner role:member role:collaborator 1of3 project:{project} 2of2"
    res = set_cpabe_policy(env.CPABE_SERVER_BASE_URL, policy, access_token)

    data = res.read()
    print('CPABE server response:', res.status, data.decode('ascii'))


def push_keys_to_keytray(ver_key, enc_key, access_token):
    print('Sending key pair to KeyTray via CPABE server')

    res = encrypt_and_upload_sse_keys(env.CPABE_SERVER_BASE_URL, ver_key, enc_key, access_token)

    data = res.read()
    print('KeyTray response:', res.status, data.decode('ascii'))

    key_id = data.decode('ascii').replace('"', '')

    return key_id


def push_ver_key_to_ta(ver_key, key_id, access_token):
    print('Uploading verification key to Trusted Authority')

    res = upload_ver_key(f"{env.SSE_TA_BASE_URL}", ver_key, key_id, access_token)

    data = res.read()
    print('TA response:', res.status, data)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("label", help="Label for new XNAT Subject", type=str)
    parser.add_argument("project", help="XNAT Project the subject will be a member of", type=str)
    args = parser.parse_args()

    print('Authenticating with KeyCloak')

    access_token = get_keycloak_access_token(
        env.KEYCLOAK_AUTH_ENDPOINT,
        env.KEYCLOAK_REALM,
        env.KEYCLOAK_PUBLIC_CLIENT,
        env.KEYCLOAK_REALM_ADMIN_USER,
        env.KEYCLOAK_REALM_ADMIN_PASSWORD,
    )


    print('Provisioning subject keys')

    set_policy(args.project, access_token)

    ver_key, enc_key = gen_keys()
    key_id = push_keys_to_keytray(ver_key, enc_key, access_token)
    push_ver_key_to_ta(ver_key, key_id, access_token)

    print('Creating XNAT subject')

    res = create_xnat_subject(
        env.XNAT_API_URL,
        env.XNAT_ADMIN_USER,
        env.XNAT_ADMIN_PASSWORD,
        args.project,
        args.label,
        key_id
    )

    data = res.read()
    print('XNAT response:', res.status, data.decode('ascii'))
