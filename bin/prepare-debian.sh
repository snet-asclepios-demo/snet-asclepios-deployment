#!/bin/bash

set -eu

# console output colors
NC='\033[0m'      # no color
RED='\033[0;31m'
GRN='\033[0;32m'
BRN='\033[0;33m'
BLU='\033[0;34m'
YLW='\033[1;33m'

# auxiliary functions
function error()  {
    if [ "$#" -lt 1 ]; then
        error "wrong number of arguments in function $FUNCNAME";
    fi
    echo -e "${RED}error: $1${NC}" >&2
    for arg in "${@:2}"; do
        echo "Hint: $arg" >&2
    done
    exit 1
}
function editing_file_msg()  {
    if [ "$#" -ne 1 ]; then
        error "wrong number of arguments in function $FUNCNAME"
    fi
    echo "editing file $1..."
}
function get_clean_config()  {
    if [ "$#" -ne 1 ]; then
        error "wrong number of arguments in function $FUNCNAME"
    fi
    if [[ ! -f "$1" && ! -f "$1.bak" ]]; then
        touch "$1"      # no file and no backup? create empty file!
    elif [[ ! -f "$1.bak" ]]; then
        cp "$1"{,.bak}  # no backup? then create one!
    else
        cp "$1"{.bak,}  # backup available? then restore before editing!
    fi
    editing_file_msg "$1"
}
function run_as_root()  {
    echo "checking privileges..."
    if [[ "$UID" -ne 0 ]] ; then
        error "please run as root"
    fi
}
function run()  {
    ssh "$TARGET" "$@"
}



# check parameters
if [ $# -ne 1 ] || [ "$1" = "-h" ] || [ "$1" == "--help" ]; then
    error "expected one argument: <user>@<host>"
fi
TARGET="$1"                            # the target to provision (syntax: <user>@<host>)
TARGET_USER=$(echo "$TARGET" | cut -d'@' -f1) # the user on the target to provision
TARGET_HOST=$(echo "$TARGET" | cut -d'@' -f2) # the target to provision (syntax: <host>.<tld>)
REPORT_EMAIL="gitlab+snet-asclepios-demo-snet-asclepios-deployment-20013-issue-@gwdg.de"  # the email to forward system mail to
UPLOAD_DIR="/usr/local/snet-asclepios-deployment"



echo "connecting to $TARGET..."
run "echo OK"




echo -e "${BLU}Secure Debian:${NC}"

echo "creating temporary directory..."
TEMP_DIR=$(run "mktemp -d")

echo "cloning secure debian project..."
run "cd $TEMP_DIR && git clone https://gitlab.com/Ilka.Schulz/secure-debian.git && cd secure-debian && git checkout master"

echo "running basic setup..."
run -tt "cd $TEMP_DIR/secure-debian && sudo ./base.sh" 1>&2

echo "running mail forwarding..."
run -tt "cd $TEMP_DIR/secure-debian && sudo ./forward-mail.sh --host=\"$TARGET_HOST\" --email=\"$REPORT_EMAIL\""

echo "removing temporary directory..."
run "rm -r \"$TEMP_DIR\""



echo -e "${BLU}GitLab Access:${NC}"

echo "checking if user for GitLab CI user exists..."
if ! run "id -u gitlab" > /dev/null; then
    echo "does not exist yet."
    echo "creating user gitlab..."
    SUGGESTED_PASSWORD=$(< /dev/urandom tr -dc 'A-Xa-x0-9' | head -c50 && echo)
    echo -e "${BRN}INFO: You can choose any password as password authentication is disabled and the user has passwordless sudo. Anyways, this one might do: $SUGGESTED_PASSWORD${NC}"
    run -tt "sudo adduser gitlab" 1>&2
else
    echo "already exists."
fi

echo "putting SSH public key..."
CI_PUB_KEY="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDGUMr2+oO1tkvcbFbJxy1v9pd2VAJ9FOMTvFNfmpWtIE+ZA2Motf3SzB/dwYYM/WELiQIRlwsbBaCKK1D6ZJ3/CXGQSffcBHl2bR4hw5Syfg7a3l3fAn1xJBb5UfwDCYYv/TQ/Qfkb/wW3jC6gjP795ZFpFQcJyFx3zI2M/iUgi1eTrfoyksRqfl4rv9dbZ/VgW22l1CUyxqNvPDoJPeSYusRJhvGP4ILDCaqOOfhmVWgNxbLyOhtja9dhEnWIbM3R2GBIg8oorgFn5ejGz4Ua6bH/oN6yiAWQCsIqpwOzfxK3dDwQZFTICeoxJyuU3OcqbDmEQdm5pqe/hLej2RTtBYIlOxIHPk7vGo5U3t1+UhHZxnMWEYleuGP/WWiBHLn1HFC7yR/jlt87iEyi4i2EXPfH4M2BTprk7tKjrvpfyKZBakHt5lOU/h2j3AMJUgEQzzQzzSBRNuIF7oSgOi4cSlv1hWegv2gOTybzDMwJ7Ia2XqKxAKKh2pfnqlg7q4cNru0b7NAP6xI/E93hW3Oh4+HazphzEjI1Qj6SK1PAdBOrS09hWxMW+0pLErerTNwqCmzu8MXhauzxDJPSPt28mdVkf0/tlZFs/4xH4LEjUoNm9lisvLfSJrFdcyUHtDEfPYbgMcPftDX5Aeru/QsSPT/rxHyfaF9ZAAAKsdMB9w== gitlab@gitlab"
run -tt "sudo mkdir -p /home/gitlab/.ssh && echo \"$CI_PUB_KEY\" | sudo tee /home/gitlab/.ssh/authorized_keys && sudo chown gitlab:gitlab -R /home/gitlab/.ssh"

echo "enabling passwordless sudo..."
run -tt "echo \"gitlab ALL=(ALL) NOPASSWD:ALL\" | sudo tee /etc/sudoers.d/gitlab-ci > /dev/null" 1>&2

echo "creating upload directory..."
run -tt "sudo mkdir -p \"$UPLOAD_DIR\" && sudo chown -R $TARGET_USER:$TARGET_USER \"$UPLOAD_DIR\"" 1>&2

echo "initializing Git repository..."
run "cd \"$UPLOAD_DIR\" && git init && git config receive.denyCurrentBranch updateInstead"

echo "giving ownership to CI user..."
run -tt "sudo chown -R gitlab:gitlab \"$UPLOAD_DIR\" && sudo chmod -R 775 \"$UPLOAD_DIR\"" 1>&2




echo -e "${BLU}Facts for manual completion:${NC}"

echo "gathering SSH host key..."
KNOWN_HOST_LINE="$TARGET_HOST "$(run "cat /etc/ssh/ssh_host_ed25519_key.pub | cut -d' ' -f1,2")  # construct line like in ~/.ssh/known_hosts

echo "creating issue on GitLab..."
REPORT="The onboarding process of the machine $TARGET_HOST is almost done. Please make sure you edit your \\\`.gitlab-ci.yml\\\` to include these jobs:

\\\`\\\`\\\`yml
push:
    extends: .push_template
    stage: push
    tags:
        # run on runners.schulz.com.de because SSH is not open worldwide^^
        - schulz.com.de
    variables:
        SSH_HOST: \"$TARGET_HOST\"
        SSH_HOST_KEY: \"$KNOWN_HOST_LINE\"
        SSH_USER: \"gitlab\"
        SSH_UPLOAD_DIR: \"$UPLOAD_DIR\"
        
configure:
    extends: .configure_gwdg_template
    tags:
        # run on runners.schulz.com.de because SSH is not open worldwide^^
        - schulz.com.de
    variables:
        SSH_HOST: \"$TARGET_HOST\"
        SSH_HOST_KEY: \"$KNOWN_HOST_LINE\"
        SSH_USER: \"gitlab\"
        SSH_UPLOAD_DIR: \"$UPLOAD_DIR\"

start:
    extends: .start-gwdg-template
    tags:
        # run on runners.schulz.com.de because SSH is not open worldwide^^
        - schulz.com.de
    variables:
        SSH_HOST: \"$TARGET_HOST\"
        SSH_HOST_KEY: \"$KNOWN_HOST_LINE\"
        SSH_USER: \"gitlab\"
        SSH_UPLOAD_DIR: \"$UPLOAD_DIR\"

test:
    extends: .test-gwdg-template
    tags:
        # run on runners.schulz.com.de because SSH is not open worldwide^^
        - schulz.com.de
    variables:
        SSH_HOST: \"$TARGET_HOST\"
        SSH_HOST_KEY: \"$KNOWN_HOST_LINE\"
        SSH_USER: \"gitlab\"
        SSH_UPLOAD_DIR: \"$UPLOAD_DIR\"

\\\`\\\`\\\`

**Hints:**
- You probably want to rename the jobs...
- here may already be a job for this machine that can be adjusted. Otherwise, you need to create a new one.

/label priority::high
/assign Ilka.Schulz
"

run "echo \"$REPORT\" | mail -s \"manual completion of onboarding of $TARGET_HOST\" root"



echo -e "${BRN}This script has generated an issue for you to manually complete setup, please visit: https://gitlab.gwdg.de/snet-asclepios-demo/snet-asclepios-deployment/-/issues/service_desk"
echo -e "${GRN}done${NC}"
